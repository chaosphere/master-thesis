# Master Thesis

This repo contains all the scripts used for my Master Thesis at the University of Padua.

## Structure of the repo

- __Evaluation__ contains all the results of the evaluation of the runs
- __Graphics__ contains all the plots drawn from the results
- __Software__ contains the scripts written in order to do the word2vec and the fusion runs (you may need some external libraries)
- __Tasks__ contains the topics, qrels and pids of the documents of the dataset
- __Terrier_Properties_Configuration__ contains the Terrier's property files used for the runs, with and without QE+RF

## Contacts

If you have any questions regarding the material hosted in this repo, please contact me at:

t.clipa _at_ pm.me

Or my Supervisor: Professor Giorgio Maria Di Nunzio at:

dinunzio _at_ dei.unipd.it
