""" This class implements a sorted dictionary sorting based 
on the key and using numpy arrays as memorizing structure """
#%%
import numpy as np

class sorted_dict:
    def __init__(self, dim=1000):
        self.dim = dim
        self.size = 0
        self._keys = np.zeros(dim, dtype=np.double)
        self._container = np.empty(dim, dtype=np.ndarray)
        self._index = 0

    def __iter__(self):
        self._index = 0
        return self
    
    def __next__(self):
        try:
            result = self._container[self._index]
        except IndexError:
            raise StopIteration
        self._index += 1
        return result

    def get_list(self):
        return np.copy(self._container)

    def reverse_bisect_right(self, a, x, lo=0, hi=None):
        """Return the index where to insert item x in list a, assuming a is sorted in descending order.

        The return value i is such that all e in a[:i] have e >= x, and all e in
        a[i:] have e < x.  So if x already appears in the list, a.insert(x) will
        insert just after the rightmost x already there.

        Optional args lo (default 0) and hi (default len(a)) bound the
        slice of a to be searched.

        Essentially, the function returns number of elements in a which are >= than x.
        >>> a = [8, 6, 5, 4, 2]
        >>> reverse_bisect_right(a, 5)
        3
        >>> a[:reverse_bisect_right(a, 5)]
        [8, 6, 5]
        """
        if lo < 0:
            raise ValueError('lo must be non-negative')
        if hi is None:
            hi = len(a)
        while lo < hi:
            mid = (lo+hi)//2
            if x > a[mid]:
                hi = mid
            else:
                lo = mid+1
        return lo

    def insert(self, score, docid):
        i = 0
        if (self.size != 0):
            i = self.reverse_bisect_right(self._keys, score)

        if (i < len(self._container)) and (i >= 0):
            tmp = np.empty(self.dim - i - 1, dtype=np.ndarray)
            keys_tmp = np.zeros(self.dim - i - 1, dtype=np.double)

            np.copyto(tmp, self._container[i:self.dim-1])
            np.copyto(keys_tmp, self._keys[i:self.dim-1])

            self._container[i] = np.array([score, docid], dtype = np.double)
            self._keys[i] = score

            if (len(tmp) != 0):
                np.copyto(self._container[i + 1:], tmp)
                np.copyto(self._keys[i + 1:], keys_tmp)

            self.size += 1
            return True
        else: 
            return False
#%%
