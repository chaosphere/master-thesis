#%% Bioclean function used to create the the tokens for the creation of the word vectors
import re

def bioclean(t): return re.sub('[.,?;*!%^&_+’\'():-\[\]{}]', '', t.replace(
    '"', '').replace('/', '').replace('\\', '').replace("'", '').replace("’", ' ').strip().lower()).split()

#%% Writes a TREC-compatible .res file, given the filename and the list/array with docid and score
from smart_open import open
def write_res(filename, scores, topicid, model="word2vec"):
    # appends the result to the file
    with open(filename, 'a') as file:
        i = 0
        for result in scores:
            # Writes the result in the trec-like type to the file
            docid = result[1]
            score = result[0][0]
            file.write("{} Q0 {} {} {} {} \n".format(topicid, int(docid), i, score, model))
            i += 1

