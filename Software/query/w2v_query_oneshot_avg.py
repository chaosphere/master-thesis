#%% Imports etc.
import time
from multiprocessing import Process, Pool
import os
from os.path import isfile, join
from sklearn.metrics.pairwise import cosine_similarity 
import numpy as np
import random
from gensim.models import KeyedVectors
from smart_open import open
from natsort import natsorted
import sys
import pickle
import math
from sorted_dict import sorted_dict
from utilities_functions import bioclean, write_res
import heapq as hq

#%% Loads the model
print("Loading w2v model...")
model = KeyedVectors.load_word2vec_format("", binary=True)
print("Model loaded!")
#%%
vec_avg_path = ""
print("Creating and sorting vector list...")

vec_avg = [f for f in os.listdir(vec_avg_path) if (
    isfile(join(vec_avg_path, f)))]
vec_avg = natsorted(vec_avg)
print("File list created and sorted!")

# Change this to change the number of documents retrieved per query
docs_per_topic_res = 1000

# Use a sorted_dict to memorize score/docid
res = sorted_dict(dim=docs_per_topic_res)
print("Creating queries representation...")
queries = []
with open("Topics file here", "r") as query_file:
    for line in query_file:
        if ("<num>" in line):
            docid = line[line.index(">")+1:].strip()
        if (" " in line):
            query = line[1:].strip()
        if ("\n" == line):
            # adds the query to the list
            queries.append([docid, query])

# word2vec avg representation of the query
queries_vec_avg = []

# Counts the queries examined
i = 0
for query in queries:
    # First, creates the representation of the query
    query_tokens = bioclean(query[1])
    # avg representation
    query_vec_avg = np.array([np.mean([model[w] for w in query_tokens if w in model] or [
        np.zeros(200)], axis=0)])
    queries_vec_avg.append([query[0], query_vec_avg])

    i += 1
print ("Queries representations created!")

#%% Multiprocess variant
start_time = time.time()
def calc_similarities(vector_avg):
    res_list = []

    for query in queries_vec_avg:
        res_list.append([query[0], []])
    
    for file_vector in vector_avg:
        # Loads the file to read
        print("Loading file...", vec_avg_path + file_vector)
        doc_vec_avg = np.load(vec_avg_path + file_vector, allow_pickle=True)
        print("File", vec_avg_path + file_vector, "loaded!")

        print("Calculating scores for file", file_vector)
        for doc_vec in doc_vec_avg:
            i = 0
            if doc_vec is None:
                continue
            for query_vec in queries_vec_avg:
                out = res_list[i][1]
                try:
                    score = model.cosine_similarities(doc_vec[1:], query_vec[1].reshape(1, -1))
                except (FloatingPointError):
                    score = 0
                if (len(out) >= 1000):
                    hq.heappop(out)
                if (math.isnan(score)):
                    score = 0
                hq.heappush(out, [score, doc_vec[0]])
                i += 1

    return res_list

# Number of processes
number_of_proc = 3
vector_file_list = []
num_of_files = round(len(vec_avg) / number_of_proc)
for i in range (0,number_of_proc):
    vector_file_list.append([vec_avg[i * num_of_files: (i + 1) * num_of_files]])

with Pool(processes=number_of_proc) as pool:
    print("Starting pool of processes...")

    multiple_results = [pool.apply_async(
        calc_similarities, (vector_file_list[i][0],)) for i in range(number_of_proc)]
    final_results = [res.get() for res in multiple_results]

print("All results calculated!")
elapsed_time = time.time() - start_time

print("Duration", elapsed_time)

#%% We need to merge the heaps and then save the results
# File that will contain the final results
res_file = ""
i = 0
for query_vec in queries_vec_avg:
    # Merge all the results from the 3 processes
    merged_list = []
    merged_list += hq.nlargest(1000, final_results[0][i][1], key=lambda t: t[0])
    merged_list += hq.nlargest(1000, final_results[1][i][1], key=lambda t: t[0])
    merged_list += hq.nlargest(1000, final_results[2][i][1], key=lambda t: t[0])
    hq.heapify(merged_list)
    top_1000 = hq.nlargest(1000, merged_list, key=lambda t: t[0])
    
    write_res(res_file, top_1000, query_vec[0], "w2v_avg")
    i += 1
print("Results merged!")

#%%
