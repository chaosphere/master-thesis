# This script creates a bin file with the vectors of a document
#%% Loads libraries
from multiprocessing import Process, Pool
import time
from sklearn.feature_extraction.text import TfidfVectorizer
from collections import defaultdict
import os
import numpy as np
from gensim.models import KeyedVectors
from smart_open import open
from natsort import natsorted
from os.path import isfile, join
import sys
from utilities_functions import bioclean

#%% Variables, paths etc
dataset_path = ""

# Creates the file list to load
file_list = [f for f in os.listdir(
    dataset_path) if isfile(join(dataset_path, f))]
file_list = natsorted(file_list)

output_file_si = "doc_vectors_si_2019_T2_"

#%%
# Name of the filename model to load
word2vec_file = ""

# Loads the model pretrained
model = KeyedVectors.load_word2vec_format(word2vec_file, binary=True)

#%% Initializes the arrays that will contain the representations

# Number of files to save per .npy file
number_of_files = 20
# Number of articles that each text file contains
articles_per_file = 5000

#%% Creates the vectors for w2v_si with TF_IDF weighting
start_time = time.time()

def create_si_vec(file_list_):
    # List that will contain ID of the article and the vector which represents it
    doc_vec_si = np.empty(articles_per_file *
                          number_of_files, dtype=np.ndarray)
    # Counts the number of articles written to the array
    number_of_articles = 0
    i = 0
    for file in file_list_:
        # Reads the file
        print("Reading file", file)
        with open(dataset_path + file) as f:
            text = ""
            article_id = -1
            # Reads each line of the file
            for line in f:
                if ('PMID: ' in line and line.index('PMID: ') == 0):
                    article_id = int(line[line.find(' '):].strip())
                elif (('Title: ' in line and line.index('Title: ') == 0) or ('Abstract: ' in line and line.index('Abstract: ') == 0)):
                    # Accumulate the title and the abstract to the text object
                    text = text + line[line.find(' '):]
                elif ('\n' == line and article_id != -1):
                    # Finished to read the article creates the vector
                    tokens = bioclean(text)
                    text = ""
                    # Creates the vector which represents the document
                    article_vec_si = np.zeros(201, dtype=np.double)
                    if (article_id != -1):
                        article_vec_si[0] = article_id
                        article_id = -1
                    else:
                        # Should not happen
                        print("Error while reading file, no PMID detected!")
                        pass

                    try:
                        tfidf = TfidfVectorizer(analyzer=lambda x: x)
                        tfidf.fit(tokens)
                        max_idf = max(tfidf.idf_)
                        word2weight = defaultdict(lambda: max_idf,
                            [(w, tfidf.idf_[i]) for w, i in tfidf.vocabulary_.items()])

                        # Saves the docid first in the array
                        article_vec_si[1:] = np.array([np.mean([model[w] * word2weight[w] for w in tokens if w in model] or [
                                                        np.zeros(200)], axis=0)])
                    except ValueError:
                        # Ignores empty documents
                        pass
                    doc_vec_si[number_of_articles] = article_vec_si
                    number_of_articles += 1
        i += 1
        # If the next file is the 21st of the array, save the array
        if (i % number_of_files == 0):
            # Save the array to disk
            print("Saving array number", str(i // number_of_files),
                "with a total of", number_of_articles, "articles")
            np.save(output_file_si + str(i // number_of_files) + ".npy", doc_vec_si)
            # Empties the vector to make room for the others
            doc_vec_si = np.empty(articles_per_file *
                                number_of_files, dtype=np.ndarray)
            number_of_articles = 0
            print("Array saved!")

    # Save the last array, even if it is not full
    print("Saving array number", str(i // number_of_files + 1),
        "with a total of", number_of_articles, "articles")

    np.save(output_file_si + str(i // number_of_files + 1) +
            ".npy", doc_vec_si[:number_of_articles])
    print("Array saved!")

number_of_proc = 1
vector_file_list = []
num_of_files = round(len(file_list) / number_of_proc)
for i in range(0, number_of_proc):
    vector_file_list.append([file_list[i * num_of_files: (i + 1) * num_of_files]])
print("Starting pool of processes...")

with Pool(processes=number_of_proc) as pool:
    res_ = [pool.apply_async(
        create_si_vec, (vector_file_list[i][0],)) for i in range(number_of_proc)]
    r = [res.get() for res in res_]

print("All vectors calculated!")
elapsed_time = time.time() - start_time
print("Duration", elapsed_time)

#%% 
