# This script creates a bin file with the vectors of a document with self-information formula
#%% Loads libraries
import os
import numpy as np
from gensim.models import KeyedVectors
from smart_open import open
from natsort import natsorted
from os.path import isfile, join
import pickle
import sys
import re
import math
from utilities_functions import bioclean

#%% Variables, paths etc
dataset_path = ""

# Creates the file list to load
file_list = [f for f in os.listdir(
    dataset_path) if isfile(join(dataset_path, f))]
file_list = natsorted(file_list)

# Output file where to write the numpy object
output_file = "doc_vectors_avg_2019_T2_"

#%% Loads the model
# Name of the filename model to load
word2vec_file = ""

# Loads the model pretrained
model = KeyedVectors.load_word2vec_format(word2vec_file, binary=True)

#%% Variables used
articles_per_file = 5000

# Number of files to save per .npy file
number_of_files = 20

# Number of entries per vector
dims = 201

# List that will contain ID of the article and the vector which represents it
doc_vec = np.empty(articles_per_file * number_of_files, dtype=np.ndarray)

#%% Creates the vectors for the documents found in the dataset_path
# Counts the number of files read
i = 0
# Counts the number of articles written to the array
number_of_articles = 0
np.seterr(all='raise')
for file in file_list:
    print ("Reading file", file)
    with open(dataset_path + file) as f:
        # Temporary variables
        text = ""
        article_id = -1
        # Reads each line of the file
        for line in f:
            if ('PMID: ' in line and line.index('PMID: ') == 0):
                article_id = int(line[line.find(' '):].strip())
            elif (('Title: ' in line and line.index('Title: ') == 0) or ('Abstract: ' in line and line.index('Abstract: ') == 0)):
                # Accumulate the title and the abstract to the text object
                text = text + line[line.find(' '):]
            elif ('\n' == line and article_id != -1):
                # Finished to read the article creates the vector
                tokens = bioclean(text)
                text = ""

                # Creates the vector which represents the document
                article_vec = np.zeros(dims, dtype=np.double)
                if (article_id != -1):
                    article_vec[0] = article_id
                    article_id = -1
                else:
                    # Should not happen
                    print("Error while reading file, no PMID detected!")
                    pass
                
                article_vec[1:] = np.array([np.mean([model[w] for w in tokens if w in model] or [
                    np.zeros(200)], axis=0)])

                # Adds the doc representation to the docs vectors
                doc_vec[number_of_articles] = article_vec
                # Counts the articles read
                number_of_articles += 1
            else:
                continue
    i += 1
    # If the next file is the number_of_files +1 element of the array, save the array
    if (i % number_of_files == 0):
        # Save the array to disk
        print("Saving array number " + str(i // number_of_files))
        # Saves the docs representations
        np.save(output_file + str(i // number_of_files) + ".npy", doc_vec)
        # Resets the doc vecs
        doc_vec = np.empty(articles_per_file * number_of_files, dtype=np.ndarray)
        number_of_articles = 0
        print("Array saved!")

# Saves last array even if not full
print("Saving array number " + str((i // number_of_files)+1))
np.save(output_file + str((i // number_of_files)+1) + ".npy", doc_vec[:number_of_articles])
print("Array saved!")

#%%
