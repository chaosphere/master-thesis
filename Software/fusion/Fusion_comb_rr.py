""" This script combines the results of IR models and combines them in order 
    to increase the overall performance by combining the models. """
#%% Imports
import numpy as np
from os import listdir
from smart_open import open
from os.path import isfile, join
from trectools import TrecRun, TrecEval, fusion
import sys
from smart_open import open

#%%
# Name of the final file
model = ''
out_file_RR = "RR_out.res"
out_file_Comb = 'CombSUM_Out.res'


# Reads the .res files and sums the scores to produce a final file
# How many documents should be per query in the final .res file 
docs_in_final = 1000
# Path where to find the .res files
path_to_res = []

res_list = []
# Creates the list of .res files
for p in path_to_res:
    res_list.append([join(p, f)
                     for f in listdir(p) if ".res" in f])  # and model in f])

print(res_list)
# TrecRuns

runs = []

for res in res_list:
    for r in res:
        runs.append(TrecRun(r))
        print('Opening run', r)

# # Comb Fusion
out = open(out_file_Comb, 'w')
fused = fusion.combos(runs, strategy="sum", output=out, max_docs=1000, norm="minmax")
out.close()

# Reciprocal Ranking
out = open(out_file_RR, 'w')
fused = fusion.reciprocal_rank_fusion(runs ,output=out)
out.close()

#%%
