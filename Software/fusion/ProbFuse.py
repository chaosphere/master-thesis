""" Implementation of the probfuse data fusion method for IR systems """
#%% Imports

import numpy as np
from os import listdir
from smart_open import open
from os.path import isfile, join
from trectools import TrecRun, TrecEval, TrecQrel
import sys
from smart_open import open

#%% Files, paths etc
# Reads the qrels file
qrels = TrecQrel("")

# Number of topics in the qrels file
Q = len(qrels.topics())
# Training res
path_to_res = ""

# Creates the list of .res files
res_list = [f for f in listdir(path_to_res) if ".res" in f]
# Creates a list which contains the TrecRuns that we need to merge
runs = []

for res in res_list:
    runs.append(TrecRun(path_to_res + res))

query_list = runs[0].topics()

# Number of segments that each .res file needs to be divided
x = 25

# Number of documents per query in the files 
docs_per_query = 1000

# Number of documents per segment
k = docs_per_query // x
#%% First part: computes the P(d_k | m), the probability that a document d, returned in the k-th
# segment, is relevant given that it is returned from the model m
docs_prob_per_system = []
# Represents the number of relevant documents in the k-th segment of the query
rel_doc_k_seg = 0
# Computes the probabilities for each system
print("Starting calculating P(d_k | m)...\n")
for run in runs:
    print("Analyzing run " + run.get_filename())
    # Holds the probability computed for each segment in the run
    segment_prob = []
    for j in range(0,x):
        print("Starting computation of probabilities of segment " + str(j+1))
        numerator = 0
        for query in query_list:
            num_of_relevants = 0
            num_of_nrel = 0
            docs = run.get_top_documents(query, n=docs_per_query)
            segment = docs [j*k : (j+1)*k-1]
            for doc in segment:
                if (qrels.get_judgement(doc, query) > 0):
                    # Doc is relevant
                    num_of_relevants += 1
                # Uses only the evaluated docs
                #elif (qrels.get_judgement(doc, query) == 0):
                    #num_of_nrel += 1
            if (num_of_nrel + num_of_relevants != 0):
                # Two versions are possible, see the original paper for more info
                #numerator += num_of_relevants / (num_of_nrel + num_of_relevants)
                numerator += num_of_relevants / k
        segment_prob.append([j, numerator/Q])
        
    print("Run analyzed!\n")
    docs_prob_per_system.append([run.get_filename(), segment_prob])
print("Probabilities computed!\n")

#%% Second part: we fuse the runs by calculating the final score of the documents
print("Starting the fusion...")

# Eval res
path_to_res = ""

# Creates the list of .res files
res_list = [f for f in listdir(path_to_res) if ".res" in f]
# Creates a list which contains the TrecRuns that we need to merge
runs = []

for res in res_list:
    runs.append(TrecRun(path_to_res + res))

query_list = runs[0].topics()

#%% Calculates the scores

# Holds the query id and the scores
query_scores = []
for query in query_list:
    print("Analyzing query", query)
    # Holds the scores for each document in the runs
    doc_score = {}
    # Model evaluted
    j = 0
    for run in runs:
        # Get the docs
        docs = run.get_top_documents(query, n=docs_per_query)
        # Gets the probabilities of the run
        prob_of_run = docs_prob_per_system[j][1]
        seg_prob = prob_of_run[0][1]
        for doc in docs:
            score = 0
            if (doc in doc_score):
                score = doc_score[doc]
            # Index of the document in the list
            pos = docs.index(doc)
            # Computes the score based on the position of the doc in the run and its prob
            score += seg_prob / (pos // k + 1)
            # updates the score of the doc analyzed
            doc_score[doc] = score
            # Updates the docs read and the segment prob if necessary
            seg_prob = prob_of_run[pos // k][1]
        # increment the model number
        j += 1
    query_scores.append(doc_score)    
print("Fusion done!\n")

#%% Orders the docs by score
sorted_query = []
i = 0
for query in query_scores:
    query_id = query_list[i]
    # Orders the documents in the run
    sorted_res = sorted(query.items(), key=lambda x: x[1], reverse=True)
    sorted_query.append([query_id, sorted_res])
    i += 1

#%% Prints the results to a file
filename = "ProbFuse_" + str(x) + ".res"

with open(filename, 'a') as file:
    for query in sorted_query:
        res = query[1]
        res = res[:1000]
        i = 0
        for result in res:
            # Writes the result in the trec-like type to the file
            docid = result[0]
            score = result[1]
            file.write("{} Q0 {} {} {} {} \n".format(
                query[0], int(docid), i, score, "probfuse"))
            i += 1

#%%
