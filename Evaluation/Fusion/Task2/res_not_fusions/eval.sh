#!/bin/bash

# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

# Check of arguments
if [ $# -eq 0 ]
    then
        echo "No arguments detected, for usage invoke $0 -h"
        exit 1
fi

while getopts ":hq:" opt; do
    case ${opt} in
    h )
        echo "Eval script, usage:"
        echo ""
        echo "-q [file] specify the qrels file to evaluate"
        echo "-h show this help"
        echo ""
        exit 0
        ;;
    q )
        QRELS=$OPTARG
        ;;
    \? )
        echo "Invalid Option: -$OPTARG" 1>&2
        exit 1
        ;;
    : )
        echo "Invalid Option: -$OPTARG requires an argument" 1>&2
        exit 1
        ;;
    esac
done
shift $((OPTIND -1))

FOLDER="`pwd`"
EXTENSION=*.res

echo "Evaluation starting..."
for file in $FOLDER/$EXTENSION; do
    FILENAME=$(basename "$file" .res)
    echo "Valutando il file $FILENAME"
    trec_eval -q -m recall $QRELS $file >> Recall_$FILENAME.eval
    trec_eval -q -m P $QRELS $file >> Precision_$FILENAME.eval
    trec_eval -q -m ndcg_cut $QRELS $file >> NDCG_$FILENAME.eval
    trec_eval -q -m all_trec $QRELS $file >> all_trec_$FILENAME.eval
done
echo ""
echo "Evaluation done!"

